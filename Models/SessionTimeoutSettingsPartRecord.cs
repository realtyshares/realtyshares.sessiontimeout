using Orchard.ContentManagement.Records;
using Orchard.Environment.Extensions;

namespace RealtyShares.SessionTimeout.Models
{
    [OrchardFeature("RealtyShares.SessionTimeout")]
    public class SessionTimeoutSettingsPartRecord : ContentPartRecord
    {
        public const int DefaultDuration = 30;
        public virtual int SessionDuration { get; set; }
    }
}