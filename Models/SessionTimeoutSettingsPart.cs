using Orchard.ContentManagement;
using Orchard.Environment.Extensions;
using System.ComponentModel.DataAnnotations;

namespace RealtyShares.SessionTimeout.Models
{
    public class SessionTimeoutSettingsPart : ContentPart<SessionTimeoutSettingsPartRecord>
    {
        [Required, Range(1, int.MaxValue)]
        public int SessionDuration
        {
            get { return Record.SessionDuration; }
            set { Record.SessionDuration = value; }
        }
    }
}
