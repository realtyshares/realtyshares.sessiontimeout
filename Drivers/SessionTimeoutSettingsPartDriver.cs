using JetBrains.Annotations;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.Environment.Extensions;
using Orchard.Localization;
using Orchard.UI.Notify;
using RealtyShares.SessionTimeout.Models;

namespace RealtyShares.SessionTimeout.Drivers
{
    public class SessionTimeoutSettingsPartDriver : ContentPartDriver<SessionTimeoutSettingsPart>
    {
        private readonly INotifier _notifier;
        private const string TemplateName = "Parts/SessionTimeoutSettings";

        public Localizer T { get; set; }
        protected override string Prefix { get { return "SessionTimeoutSettings"; } }

        public SessionTimeoutSettingsPartDriver(INotifier notifier)
        {
            _notifier = notifier;
            T = NullLocalizer.Instance;
        }

        protected override DriverResult Editor(SessionTimeoutSettingsPart part, dynamic shapeHelper)
        {
            if (part.SessionDuration == 0) part.SessionDuration = SessionTimeoutSettingsPartRecord.DefaultDuration;
            return ContentShape("Parts_SessionTimeoutSettings_Edit",
                    () => shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: part, Prefix: Prefix))
                    .OnGroup("Users");
        }

        protected override DriverResult Editor(SessionTimeoutSettingsPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            updater.TryUpdateModel(part, Prefix, null, null);
            return Editor(part, shapeHelper);
        }

    }
}