﻿Name: Session Timeout
AntiForgery: enabled
Author: André Rieussec, RealtyShares
Website: https://bitbucket.org/realtyshares/realtyshares.sessiontimeout/overview
Version: 1.0
OrchardVersion: 1.7
Description: Lets you configure the amount of time a user can be idle before their session expires.
Features:
    RealtyShares.SessionTimeout:
        Description: Lets you configure the amount of time a user can be idle before their session expires.
		    Category: Performance
